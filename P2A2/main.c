#include <stdio.h>
#include <stdlib.h>
#include <math.h>

    

int main(int argc, char** argv) {
    double pi = 3.141592654;
    float xwert = 0.0;
    float ywert = 0.0;
    int winkel = 0;
    int radius = 0;
    int auswahl = 0;
    
    printf("PolCalc V 1.0\n\n");
    printf("Sie koennen mit diesem Programm kartesische Koordinaten\n");
    printf("in Polarkoordinaten umwandeln bzw. die entsprechende\n");
    printf("Gegenoperation durchfuehren\n\n");
    printf("Bitte waehlen Sie ihre gewuenschte Rechenoperation:\n\n");
    printf("Polarkoordinaten ==> kartesische Koordinaten\t<1>\n\n");
    printf("kartesische Koordinaten ==> Polarkoordinaten\t<2>\n\n");
    printf("Ihre Auswahl ");
    scanf("%d", &auswahl);
    
    switch (auswahl) {
        case 1: printf("\nBitte geben Sie den Winkel ein ");
                scanf("%d", &winkel);
                printf("\nBitte geben Sie den Radus ein ");
                scanf("%d", &radius);
                printf("\nDer x-Wert lautet %f", radius*cos(winkel*pi/180));
                printf("\n\nDer y-Wert lautet %f\n\n", radius*sin(winkel*pi/180));
                break;
        case 2: printf("\nBitte geben Sie die x-Koordinate ein ");
                scanf("%f", &xwert);
                printf("\nBitte geben Sie die y-Koordinate ein ");
                scanf("%f", &ywert);
                printf("\nDer Winkel betraegt %f\n", 180/pi*(atan2(ywert,xwert)));
                printf("\nDer Radius betraegt %f\n\n", sqrt(xwert*xwert+ywert*ywert));
                break;
    }
    return (EXIT_SUCCESS);
}
