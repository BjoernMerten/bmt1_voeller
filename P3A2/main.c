/* Einbinden der Verzeichnisse und definieren globaler Parameter */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define MYEPS 1.0E-19
#define MAX 3

/* Initialisieren von den Arrays Matrix und Vektor */

float matrix[MAX][MAX]={0.0};
float vektor[MAX]={0.0};
float erg[MAX]={0.0};

/* Auflisten aller Funktionen des Programms */

void MatrixEinlesen();
void VektorEinlesen();

void MatrixAusgeben();
/*void VektorAusgeben();*/

int VorwaertsEliminieren();
void RueckwaertsEinsetzen();

/* Schreiben der Funktionen */

void MatrixEinlesen()
{
     printf("Bitte geben Sie die Matrix zeilenweise nach folgendem Muster ein:\n\n"
            "Wert 1 [Leerzeile] Wert 2 [Leerzeile] Wert 3 [Enter]\n\n"
            "Bitte achten Sie auf das jeweilige Vorzeichen des Wertes!!!\n\n");
    
    
    for(int zeile=0;zeile<MAX;zeile++)
    {
        printf("Bitte geben Sie die %i. Zeile ein:\n", zeile+1);
        for(int spalte=0;spalte<MAX;spalte++)
        {
            scanf("%f", &matrix[zeile][spalte]);
        }
    }
   
    fflush(stdin);
}

void VektorEinlesen()
{
    printf("\nBitte geben Sie nun den Ergebnisvektor nach folgendem Muster ein:\n\n"
            "Vektor Zeile 1 [Enter]\n"
            "Vektor Zeile 2 [Enter]\n"
            "Vektor Zeile 3 [Enter]\n\n"
            "Bitte achten Sie auf das jeweilige Vorzeichen des Wertes!!!\n\n");
    for(int zeile=0;zeile<MAX;zeile++)
    {
        scanf("%f", &vektor[zeile]);
    }
    
    fflush(stdin);
    system("cls");
}
                
void MatrixAusgeben()
{
    printf("Sie haben folgende Matrix und folgenden Vektor eingegeben:\n");
    
    for(int zeile=0;zeile<MAX;zeile++)
    {
        for(int spalte=0;spalte<MAX;spalte++)
        {
            printf("%.2f\t", matrix[zeile][spalte]);
        }
            printf("*\t%.2f\n", vektor[zeile]);
}
}
/*void VektorAusgeben()
{
    printf("Sie haben folgenden Vektor eingegeben:\n");
    
    for(int zeile=0;zeile<MAX;zeile++)
    {
        printf("%.1.2f\n", vektor[zeile]);
    }
}*/

int  VorwaertsEliminieren()
{
    float mul=0.0;
    int test=-1;
    int varz=1;
    int zeile=1;
    int spalte=1;
    int close=0;
    
  for(varz=1;varz<MAX;varz++)
    {
      for(zeile=varz;zeile<MAX;zeile++)
        {
           if(matrix[zeile][varz-1]<MYEPS)
            {
                close=1;
                break;
            }
            mul=(matrix[varz-1][varz-1]/matrix[zeile][varz-1])*(-1);
                for(spalte=varz-1;spalte<MAX;spalte++)
                  {
                        matrix[zeile][spalte]=(matrix[zeile][spalte])*(mul);
                        matrix[zeile][spalte]=(matrix[varz-1][spalte])+(matrix[zeile][spalte]);
                  }
                        vektor[zeile]=vektor[zeile]*(mul);
                        vektor[zeile]=vektor[varz-1]+vektor[zeile];
        }
           if(close==1)
            {
            printf("\n**********************************************\n"
                     "* Pivotelement gleich Null - Programmabbruch *\n"
                     "**********************************************\n");
            break;
            }
    }
   
    if(close<MYEPS)
    {
        for(int zeile=MAX-1;zeile>0;zeile--)
          {
             for(int spalte=zeile-1;spalte>=0;spalte--)
             { if (matrix[zeile][spalte]==0)
                test=test*0;
             }
          }
    }
        return (test);    
}

void RueckwaertsEinsetzen()
{             
    int par=MAX-1;
    
    for(int zeile=2;zeile>=0;zeile--)
    {
                for(int i=par;i>zeile;i--)
                {
                matrix[zeile][i]=matrix[zeile][i]*erg[i];  
                vektor[zeile]=vektor[zeile]-matrix[zeile][i];
                }
    erg[zeile]=vektor[zeile]/matrix[zeile][zeile];
    }
    
    }


int main() {

    int run;
    
    printf("MathCalc Matrix V 1.0\n");
    printf("Mit diesem Programm kann eine 3x3 Matrix mit drei Unbekannten gelöst werden.\n\n");
    
    MatrixEinlesen();
    VektorEinlesen();
    
    MatrixAusgeben();
    /*VektorAusgeben();*/
    
    run=VorwaertsEliminieren();
    if(run==0)
    {
    RueckwaertsEinsetzen();
    
    
    }
    
    
    return (EXIT_SUCCESS);
}
