#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(void)
{
    const float pi = 3.1415927;
    float radius;
    float oberflaeche;
    float volumen; 
    
    printf("Kugelradius in Metern: ");
    scanf("%f", &radius);
    oberflaeche = 4 * pi * pow(radius , 2);
    printf("Obererfläche im Quadratmetern: %f\n ",oberflaeche);
    volumen = (4.0/3) * pi * pow(radius, 3);
    printf("Volumen in Kubikmetern: %f\n", volumen );
    
    
    return (0);
}