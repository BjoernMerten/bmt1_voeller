#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <time.h>
#include <math.h>
#include <windows.h>    

#define MAXZ 30     
#define MAXS 30
#define LIFECYCLES 100 
#define MYEPS 1.0E-19

char arra[MAXZ][MAXS]={' '};
char arrb[MAXZ][MAXS]={' '};

/* Prototyping */

void MatrixEinlesen();

void MatrixAusgeben();

void GoLife();

int CheckCell(int i, int j);

int CheckNeigbour(int i, int j);

int Evolution(int neigbour, int live);

void FillMatrix(int survive, int i, int j);

void CopyMatrix();

void ManuellesEingeben();

void FigurenAuswahl();

void ZufaelligesSetzen();

void FigurenSetzen();

void _sleep(int a);

int main(int argc, char *argv[]) 
{
    printf("Game of Life v 1.0\n");
    
    MatrixEinlesen();
    
    MatrixAusgeben();

    GoLife();
    
  return 0;
}

/* Funktionen schreiben */

void MatrixEinlesen()
{
    int auswahl=0;
    fflush(stdin);
    
    printf("Die Lebensumgebung betraegt 30 x 30 Felder, beginnend bei 0.\n"
           "Bitte waehlen Sie die Moeglichkeit, wie sie ihre lebenden Zellen\n"
           "setzen wollen, aus den folgenden drei Moeglichkeiten aus:\n\n"
           "\t1. Manuelles setzen jeder einzelnen Zelle\n\n"
           "\t2. Setzen von vorgefertigten Figuren\n\n"
           "\t3. Zufaelliges setzen\n\n"
           "\tIhre Auswahl: ");
    
    scanf("%i", &auswahl);
    
    system("cls");
    
    switch(auswahl)
    {
        case 1: ManuellesEingeben();break;
        case 2: FigurenAuswahl();break;
        case 3: ZufaelligesSetzen();break;
    }
    system("cls");
}

void MatrixAusgeben()
{
    system("cls");
    for(int x=0;x<MAXZ;x++)
            {
                for(int y=0;y<MAXS;y++)
                {
                         printf("%c",arra[x][y]);
                }
                printf("\n");
        
    }
    
    _sleep(150);
    
}

void GoLife()
{
    int life=0;
    int neigbour=0;
    int survive=0;
   
    
    for(int t=0;t<LIFECYCLES;t++)
    {
        for(int i=0;i<MAXZ;i++)
        {
            for(int j=0;j<MAXS;j++)
            {
                life=CheckCell(i,j);
                neigbour=CheckNeigbour(i,j);
                survive=Evolution(neigbour, life);
                FillMatrix(survive,i,j);
                
                
            }
        
        }
        CopyMatrix();
        MatrixAusgeben();
    }
        
}

int CheckCell(int i, int j)
{
    int life=0;
    if(arra[i][j]=='*')
    {
        life=1;
    }
    else
    {
        life=0;
    }
    return (life);
}

int CheckNeigbour(int i,int j)
{
    int neigbour=0;
    
    if (arra[(i-1+MAXZ) % MAXZ][(j-1+MAXS) % MAXS]=='*')
    {
        neigbour=neigbour+1;
    }
    if (arra[(i-1+MAXZ) % MAXZ][j]=='*')
    {
        neigbour=neigbour+1;
    }
    if (arra[(i-1+MAXZ) % MAXZ][(j+1+MAXS) % MAXS]=='*')
    {
        neigbour=neigbour+1;
    }
    if (arra[i][(j-1+MAXS) % MAXS]=='*')
    {
        neigbour=neigbour+1;
    }
    if (arra[i][(j+1+MAXS) % MAXS]=='*')
    {
        neigbour=neigbour+1;
    }
    if (arra[(i+1+MAXZ) % MAXZ][(j-1+MAXS) % MAXS]=='*')
    {
        neigbour=neigbour+1;
    }
    if (arra[(i+1+MAXZ) % MAXZ][j]=='*')
    {
        neigbour=neigbour+1;
    }
    if (arra[(i+1+MAXZ) % MAXZ][(j+1+MAXS) % MAXS]=='*')
    {
        neigbour=neigbour+1;
    }
    return(neigbour);
}

int Evolution(int neigbour, int life)
{
    int survive=0;
    
    if(life==1)
    { switch (neigbour)
        {
                case 2: survive=1; break;
                case 3: survive=1; break;
        }
    }
    else
    {
        if(neigbour==3)
        {
            survive=1;
        } 
    }
    return(survive);
}

void FillMatrix(int survive, int i, int j)
{
    if(survive==1)
    {arrb[i][j]='*';}
    else
    {arrb[i][j]=' ';}   
}
    
void CopyMatrix()
{
    for(int i=0;i<MAXZ;i++)
    {
        for (int j=0;j<MAXS;j++)
        {
            arra[i][j]=arrb[i][j];
        }
    }
}

void ManuellesEingeben()
{  
    int z;
    int s;
    fflush(stdin);
    system("cls");
    
    
            printf("Bitte geben geben Sie die Felder, in denen sich lebende Zellen befinden\n\n"
                   "sollen nach folgendem Muster ein:\n\n\t\t Zeile [LEERZEICHEN] Spalte [Enter].\n\n"
                   "Der Buchstabe \"E\" beendet die Eingabe.\n");
    
    while(scanf("%i\t%i\n", &z,&s)!=0)
    {
        if(z<0||z>MAXZ||s<0||s>MAXS)
        {
            printf("Sie wollten eine lebende Zelle ausserhalb der Lebensumgebung setzen,\n"
                    "bitte wiederholen Sie ihre Eingabe.\n");
        }
        else
        {
                    arra[z][s]='*';
        }
    }  
}

void FigurenAuswahl(int figur)
{
    fflush(stdin);
    int i=0;
    int j=0;
    
    
    printf("Sie haben folgende vorgefertigte Figuren zur Auswahl:\n\n"
           "                               *      \n"
           "                             **       \n" 
           "     ***                      **      \n"
           "     * *                              \n" 
           "     * *          **       *          \n" 
           "                 **         **        \n" 
           "     * *          *        **         \n" 
           "     * *                       ***    \n"
           "     ***                         *    \n"
           "                                *     \n"
           "                                      \n"
           "   Figur 1     Figur 2     Figur 3    \n\n");
    
    printf("Bitte waehlen Sie eine Figur und geben die Nummer ein:\t");
    scanf("%i", &figur);
    printf("\nBitte waehlen sie die ein Feld, in das die linke obere Ecke der\n"
           "Figur gesetzt werden soll und beachten folgendes Muster:\n\n"
           "\tZeile [LEERZEICHEN] Spalte [Enter]\n\n\t");
    scanf("%i\t%i", &i,&j);
    FigurenSetzen(figur,i,j);
    
}

void FigurenSetzen(int figur, int i, int j)
{
    switch(figur)
    {
        case 1:
            arra[i][j]='*';
            arra[i][j+1]='*';
            arra[i][j+2]='*';
            arra[i+1][j]='*';
            arra[i+1][j+2]='*';
            arra[i+2][j]='*';
            arra[i+2][j+2]='*';
            arra[i+4][j]='*';
            arra[i+4][j+2]='*';
            arra[i+5][j]='*';
            arra[i+5][j+2]='*';
            arra[i+6][j]='*';
            arra[i+6][j+1]='*';
            arra[i+6][j+2]='*';
            break;
            
        case 2:
            arra[i][j+1]='*';
            arra[i][j+2]='*';
            arra[i+1][j]='*';
            arra[i+1][j+1]='*';
            arra[i+2][j+1]='*';
            break;
            
        case 3:
            arra[i][j+4]='*';
            arra[i+1][j+2]='*';
            arra[i+1][j+3]='*';
            arra[i+2][j+3]='*';
            arra[i+2][j+4]='*';
            arra[i+4][j]='*';
            arra[i+5][j+1]='*';
            arra[i+5][j+2]='*';
            arra[i+6][j]='*';
            arra[i+6][j+1]='*';
            arra[i+7][j+4]='*';
            arra[i+7][j+5]='*';
            arra[i+7][j+6]='*';
            arra[i+8][j+6]='*';
            arra[i+9][j+5]='*';
            break;
    }  
}

void ZufaelligesSetzen()
{   int i;
    int j;
    int arrc[MAXZ][MAXS];
    
    srand((unsigned)time(NULL));
    
    for(i=0;i<MAXZ;i++)
    {
        for(j=0;j<MAXS;j++)
        {
            arrc[i][j]=rand()%10;
            switch (arrc[i][j])
            {
                case 0:arrc[i][j]=0;break;
                case 1:arrc[i][j]=1;break;
                case 2:arrc[i][j]=0;break;
                case 3:arrc[i][j]=0;break;
                case 4:arrc[i][j]=1;break;
                case 5:arrc[i][j]=0;break;
                case 6:arrc[i][j]=0;break;
                case 7:arrc[i][j]=1;break;
                case 8:arrc[i][j]=0;break;
                case 9:arrc[i][j]=0;break;
            }
            if(arrc[i][j]==1)
            {
                arra[i][j]='*';
            }
            else
            {
                arra[i][j]=' ';
            }      
        }   
    }
}       