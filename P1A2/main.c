#include <stdio.h>
#include <stdlib.h>
#include <math.h> /* für pow(x,y) */

/*
 * 
 */
int main(void) {

    const float pi = 3.1415927;
    float radius;
    float oberflaeche;
    float volumen;
    float hoehe;
    
    printf("Radius in Metern: ");
    scanf("%f", &radius);
    
    printf("Hoehe in Metern: ");
    scanf("%f", &hoehe);
    
    oberflaeche = ( 2 * pi * pow(radius,2)) + ( 2 * pi * radius * hoehe);
    printf("Oberflaeche in Quadratmetern: %f\n", oberflaeche);
    
    volumen = ( pow(radius,2) * pi * hoehe);
    printf("Volumen in Kubikmetern: %f\n", volumen);
    
    return (0);
}

